%%%-------------------------------------------------------------------
%%% @author Marco K
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 06. Dec 2018 4:54 PM
%%%-------------------------------------------------------------------
-module(chat_erlbus_handler).
-author("Marco K").

%% API
-export([handle_msg/2]).

handle_msg(Msg, Context) ->
  Context ! {message_published, Msg}.
